import React from 'react';
import { Grid } from "semantic-ui-react";
import './App.css';
import ColorPanel from "./ColorPanel/ColorPanel";
import Messages from "./Messages/Messages";
import SidePanel from "./SidePanel/SidePanel";
import MetaPanel from "./MetaPanel/MetaPnel";
import { connect } from "react-redux";

const App = ({ currentUser, currentChannel, isPrivateChannel, userPosts, primaryColor, secondaryColor }) => (
  <Grid className="app" columns="equal" style={{ backgroud: secondaryColor }}>
    <ColorPanel 
      currentUser={currentUser}
      key={currentUser && currentUser.name}
    />
    <SidePanel 
      currentUser={ currentUser } 
      key={currentUser && currentUser.uid}
      primaryColor={primaryColor}
    />
    <Grid.Column style={{ marginLeft: 320 }}>
      < Messages 
        key={currentChannel && currentChannel.id}
        currentChannel={currentChannel}
        currentUser={currentUser}
        isPrivateChannel={isPrivateChannel}
      />
    </Grid.Column>
    <Grid.Column width={4}>
      <MetaPanel
        isPrivateChannel={isPrivateChannel}
        key={ currentChannel && currentChannel.name }
        currentChannel={currentChannel} 
        userPost={userPosts}
      />
    </Grid.Column>
  </Grid>
);

const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
  currentChannel: state.channel.currentChannel,
  isPrivateChannel: state.channel.isPrivateChannel,
  userPosts: state.channel.userPosts,
  primaryColor: state.colors.primaryColor,
  secondaryColor: state.colors.secondaryColor,
})

export default connect(mapStateToProps)(App);
