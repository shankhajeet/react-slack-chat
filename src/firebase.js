import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

var config = {
    apiKey: "AIzaSyDCzby8IE3iRHFtJTd5NgXvUhHbtAV1UO8",
    authDomain: "react-chat-web-app-8888.firebaseapp.com",
    databaseURL: "https://react-chat-web-app-8888.firebaseio.com",
    projectId: "react-chat-web-app-8888",
    storageBucket: "react-chat-web-app-8888.appspot.com",
    messagingSenderId: "1076552471463"
};

firebase.initializeApp(config);

export default firebase;
